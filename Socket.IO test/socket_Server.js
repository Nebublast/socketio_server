var bodyparser = require('body-parser');
var io = require('socket.io')(process.env.PORT || 6690);

console.log('Server Initialized');

io.on('connection', function (socket) {

    console.log('Client connected');
    socket.emit('Welcome to Socket.IO server.');

    socket.on('SendingMessage', function (data) {

        var string_data = JSON.stringify(data);

        console.log('receiving message', JSON.stringify(data));
    });
});